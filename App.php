<?php

namespace HakimCh\Wordpress;

class App
{
    public static function get($key, $array)
    {
        $parsed = explode('.', $key);
        while ($parsed) {
            $next = array_shift($parsed);
            if (is_array($array) && array_key_exists($next, $array)) {
                $array = $array[$next];
            } else {
                return null;
            }
        }
        return $array;
    }

    public static function fromCamelCase($input)
    {
        preg_match_all('!([A-Z][A-Z0-9]*(?=$|[A-Z][a-z0-9])|[A-Za-z][a-z0-9]+)!', $input, $matches);
        $ret = $matches[0];
        foreach ($ret as &$match) {
            $match = $match == strtoupper($match) ? strtolower($match) : lcfirst($match);
        }
        return implode('_', $ret);
    }
}
