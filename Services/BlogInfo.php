<?php

namespace HakimCh\Wordpress\Services;

use HakimCh\Wordpress\App;

class BlogInfo
{
    private static $functions = [];

    public function get($name)
    {
        return get_bloginfo($name);
    }

    public static function __callStatic($method, $arguments)
    {
        if (array_key_exists($method, self::$functions)) {
            $argument = self::$functions[$method];
        } else {
            self::$functions[$method] = $argument = App::fromCamelCase(
                str_replace('get', '', $method)
            );
        }
        return call_user_func_array([self::class, 'get'], [$argument]);
    }
}
