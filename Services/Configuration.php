<?php

namespace HakimCh\Wordpress\Services;

use HakimCh\Wordpress\Exceptions\ConfigurationException;
use Symfony\Component\Yaml\Yaml;

class Configuration
{
    public static function load($fileName = 'theme')
    {
        $fileDump = self::parse($fileName);
        return new ArrayBag($fileDump);
    }

    public static function parameters($fileName = 'theme')
    {
        $fileDump = self::parse($fileName);
        return new ArrayBag($fileDump['parameters']);
    }

    private static function parse($fileName)
    {
        if (!file_exists($filePath = get_template_directory() . '/app/' . $fileName . '.yml')) {
            throw new ConfigurationException($fileName . ' not found in app directory');
        }

        $fileContent = file_get_contents($filePath);
        return Yaml::parse($fileContent);
    }
}
