<?php

namespace HakimCh\Wordpress\Services;

use HakimCh\Wordpress\Exceptions\EnqueueException;

class Enqueue
{
    private static $instance;
    private static $queue;

    public function add($type, $src, $for = 'all')
    {
        $handle = basename($src);
        self::$queue[$for][$type][$handle] = [$handle, $src];
    }

    public function process($queue)
    {
        if (!is_array($queue) || empty($queue)) {
            throw new EnqueueException('The queue is not an array or empty');
        }

        foreach ($queue as $link) {
            list($type, $src, $for) = $link;
            $handle = basename($src);
            self::$queue[$for][$type][$handle] = [$handle, self::asset($src)];
        }

        add_action('wp_enqueue_scripts', function () {
            self::wpEnqueue('all', 'style');

            if (!is_admin()) {
                self::wpEnqueue('front', 'style');
            } else {
                self::wpEnqueue('admin', 'style');
            }

            self::wpEnqueue('all', 'script');

            if (!is_admin()) {
                self::wpEnqueue('front', 'script');
            } else {
                self::wpEnqueue('admin', 'script');
            }
        });
    }

    private static function wpEnqueue($for, $type)
    {
        if (isset(self::$queue[$for][$type]) && !empty(self::$queue[$for][$type])) {
            $functionName = 'wp_enqueue_' . $type;
            foreach (self::$queue[$for][$type] as $link) {
                list($handle, $src) = $link;
                call_user_func_array($functionName, [$handle, $src]);
            }
        }
    }

    private static function asset($src)
    {
        if (strpos($src, 'http') === false) {
            $src = asset($src);
        }
        return $src;
    }

    public static function __callStatic($method, $arguments)
    {
        if (!self::$instance) {
            self::$instance = new Enqueue();
        }

        return call_user_func_array([self::$instance, $method], $arguments);
    }
}
