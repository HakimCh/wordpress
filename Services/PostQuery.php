<?php

namespace HakimCh\Wordpress\Services;

use HakimCh\Wordpress\Contracts\DataQueryInterface;
use HakimCh\Wordpress\Exceptions\PostException;

class PostQuery implements DataQueryInterface
{

    /**
     * @var PostQuery $instance
     */
    private static $instance;

    /**
     * @var string $type
     */
    private static $type;

    /**
     * @var array $posts
     */
    public $posts;

    /**
     * Post Criteria
     *
     * @var array $args
     */
    private $args = [];

    /**
     * Allowed post matching criteria
     *
     * @var array $allowedFields
     */
    private $allowedFields = array(
        'numberposts', 'category', 'orderby', 'order', 'include', 'exclude', 'meta_value',
        'suppress_filters', 'post_status', 'post_type', 'numberposts', 'posts_per_page', 'tax_query',
        'category', 'include', 'cat', 'post__in', 'tag__in', 'post__not_in', 'caller_get_posts', 's',
        'offset', 'taxonomy', 'meta_key', 'meta_query'
    );

    /**
     * Add post type to the query
     *
     * @param string $type
     *
     * @return PostQuery
     */
    public static function type($type)
    {
        if (!self::$instance) {
            self::$instance = new PostQuery();
        }
        self::$type = $type;
        return self::$instance->where('post_type', $type);
    }

    /**
     * Add a condition to the post query
     *
     * @param string $field
     * @param mixed $value
     *
     * @return PostQuery
     * @throws PostException
     */
    public function where($field, $value)
    {
        if (!in_array($field, $this->allowedFields)) {
            throw new PostException($field . ' field is not allowed');
        }
        $this->args[$field] = $value;
        return $this;
    }

    /**
     * Add an offset to the post query
     *
     * @param int $offset
     *
     * @return PostQuery
     */
    public function offset($offset = 0)
    {
        return $this->where('offset', $offset);
    }

    /**
     * Add how many posy to show per page
     *
     * @param int $perPage
     *
     * @return PostQuery
     */
    public function take($perPage = 9)
    {
        return $this->where('posts_per_page', $perPage);
    }

    /**
     * Order the posts
     *
     * @param string $orderBy
     * @param string $order
     *
     * @return PostQuery
     */
    public function orderBy($orderBy = 'date', $order = 'DESC')
    {
        return $this->where('orderby', $orderBy)
                    ->where('order', $order);
    }

    /**
     * Get the list of posts
     *
     * @return \WP_Post
     */
    public function get()
    {
        $this->posts = get_posts($this->args);
        $posts = $this->posts;
        $this->args = [];
        return $posts;
    }

    /**
     * Get the posts count
     *
     * @param string $perm
     *
     * @return integer
     */
    public function count($perm = "")
    {
        return wp_count_posts(self::$type, $perm);
    }
}
