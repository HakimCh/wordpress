<?php

namespace HakimCh\Wordpress\Services\Renderer;

class PHPRenderer extends Renderer
{

    /**
     * Render the html
     *
     * @return string
     */
    public function render()
    {
        ob_start();
        extract($this->globals);
        require $this->template;
        $content = ob_get_contents();
        ob_clean();

        return $content;
    }
}
