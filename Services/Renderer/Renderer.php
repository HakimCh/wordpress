<?php

namespace HakimCh\Wordpress\Services\Renderer;

abstract class Renderer
{
    protected $template;
    protected $globals = [];

    /**
     * Add global variables to the view
     *
     * @param array $globals
     *
     * @return void
     */
    public function setGlobals(array $globals)
    {
        $this->globals = $globals;
    }

    /**
     * Set the template to render
     *
     * @param string $template
     *
     * @return void
     */
    public function setTemplate($template)
    {
        $this->template = $template;
    }

    /**
     * Render the html
     *
     * @return string
     */
    abstract public function render();
}
