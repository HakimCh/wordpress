<?php

namespace HakimCh\Wordpress\Services;

use HakimCh\Wordpress\Traits\Post;
use WP_Post;

class SinglePost
{
    use Post;

    /**
     * @var ArrayBag $post
     */
    private $post;

    /**
     * @var PostMetaBox $meta
     */
    private $meta;

    /**
     * SinglePost constructor.
     * @param WP_Post|string|null $wpPost
     */
    public function __construct($post = null)
    {
        if (!$post) {
            $post = is_string($post) ? get_page_by_path($post) : get_post();
        }

        $this->post = new ArrayBag($this->toArray($post));
        $this->meta = new PostMetaBox($post->ID);

        if (function_exists('post_view_tracker')) {
            post_view_tracker($post->ID);
        }
    }

    public function getPost()
    {
        $post = $this->post;
        $post->set('metas', $this->meta);
        return $post;
    }

    public function getMeta($name, $isImage = null)
    {
        return $this->meta->get($name, $isImage);
    }

    public function getLinkByPath($path)
    {
        return get_permalink(get_page_by_path($path));
    }

    public function getId()
    {
        return $this->post->get('ID');
    }

    public function getSlug()
    {
        return $this->post->get('post_name');
    }

    public function getType()
    {
        return $this->post->get('post_type');
    }

    public function getMetaFormater($name)
    {
        return str_replace(["[b]", "[/b]"], ["<strong>", "</strong>"], $this->meta->get($name, null));
    }

    public function getTitle()
    {
        return $this->post->get('post_title');
    }

    public function getUnderTitle()
    {
        return $this->meta->get('under_title');
    }

    public function getContent()
    {
        return $this->post->get('post_content');
    }

    public function getTemplate()
    {
        return $this->post->get('post_template');
    }

    /**
     * @return mixed
     */
    public function getAuthorName()
    {
        return get_the_author_meta('display_name', $this->post->get('post_author'));
    }

    /**
     * @return mixed
     */
    public function getAuthorBio()
    {
        return get_the_author_meta('description', $this->post->get('post_author'));
    }

    /**
     * @return mixed
     */
    public function getExcerpt()
    {
        return $this->post->get('post_excerpt');
    }

    /**
     * @return mixed
     */
    public function getAuthorImage()
    {
        return get_avatar_url($this->post->get('post_author'));
    }

    /**
     * @return mixed
     */
    public function getCategory()
    {
        $categories = [];
        $rawCategory = $this->post->get('post_category');
        if (is_array($rawCategory)) {
            foreach ($rawCategory as $category) {
                $categories[get_cat_name($category)] = get_category_link($category);
            }
        } else {
            $categories[get_cat_name($rawCategory)] = get_category_link($rawCategory);
        }
        return $categories;
    }

    /**
     * @param bool $style
     * @param string $default
     *
     * @return mixed
     */
    public function getImage($style = false, $default = 'motif.jpg')
    {
        $imageUrl = get_the_post_thumbnail_url($this->post->get('ID'));
        if ($style && !$imageUrl) {
            $imageUrl = asset('images/' . $default);
        }
        return $imageUrl;
    }

    /**
     * @param string $format
     *
     * @return mixed
     */
    public function getCreatedAt($format = 'd/m/Y H:i', $date = null)
    {
        if (!$date) {
            $date = $this->post->get('post_date');
        }

        $date = new \DateTime($date);
        return $date->format($format);
    }

    public function getMetaDate($name, $format = 'd/m/Y H:i')
    {
        return $this->getCreatedAt($format, $this->getMeta($name));
    }

    public function getViews()
    {
        if (!$views = $this->getMeta('views')) {
            $views = 0;
        }
        return $views;
    }

    public function getTags()
    {
        return get_the_tags();
    }

    public function getArrayTags()
    {
        $tags = [];
        $wpTags = wp_get_post_tags($this->post->get('ID'));
        foreach ($wpTags as $tag) {
            $tags[] = $tag->term_id;
        }
        return $tags;
    }

    public function getPermalink()
    {
        return get_permalink($this->post->get('ID'));
    }

    public function getRelatedPosts($perPage = 3)
    {
        $tags = $this->getArrayTags();
        if ($tags) {
            $post = new UserQuery();
            return $post->type('post')
                ->where('tag__in', $tags)
                ->where('post_status', 'publish')
                ->where('post__not_in', [$this->post->get('ID')])
                ->where('caller_get_posts', 1)
                ->take($perPage)
                ->get();
        }
        return [];
    }
}
