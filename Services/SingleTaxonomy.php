<?php
/**
 * Created by PhpStorm.
 * User: hakim
 * Date: 27/08/17
 * Time: 20:58
 */

namespace HakimCh\Wordpress\Services;

use WP_Term;

class SingleTaxonomy
{

    /**
     * @var ArrayBag $post
     */
    private $term;

    /**
     * @var PostMetaBox $meta
     */
    private $meta;

    /**
     * SingleTaxonomy constructor.
     * @param WP_Term|string|null $wpPost
     */
    public function __construct($term = null)
    {
        $term = get_term($term);

        $this->term = new ArrayBag((array)$term);
        $this->meta = new PostMetaBox($term->term_id, true);
    }

    public function getTerm()
    {
        $term = $this->term;
        $term->set('metas', $this->meta);
        return $term;
    }

    public function getMeta($name, $isImage = null)
    {
        return $this->meta->get($name, $isImage);
    }

    public function getId()
    {
        return $this->term->get('term_id');
    }

    public function getTitle()
    {
        return $this->term->get('name');
    }

    public function getSlug()
    {
        return $this->term->get('slug');
    }

    public function getDescription()
    {
        return $this->term->get('description');
    }

    public function getImage($name = 'image')
    {
        return $this->getMeta($name, true);
    }

    public function getPostCount()
    {
        return $this->term->get('count');
    }
}
