<?php

namespace HakimCh\Wordpress\Services;

use HakimCh\Wordpress\Exceptions\ThemeException;
use HakimCh\Wordpress\Exceptions\ViewException;
use Symfony\Component\Yaml\Yaml;

class Theme
{
    public static function logo()
    {
        $custom_logo_id = get_theme_mod('custom_logo');
        $image = wp_get_attachment_image_src($custom_logo_id, 'full');
        return $image[0];
    }

    /**
     * Setup the theme
     *
     * @throws ViewException
     * @return void
     */
    public static function setup()
    {
        $options = Configuration::load('theme')->get('parameters');
        if (array_key_exists('features', $options) && !empty($options['features'])) {
            foreach ($options['features'] as $feature) {
                add_theme_support($feature);
            }
        }
        if (array_key_exists('remove_filter', $options) && !empty($options['remove_filter'])) {
            foreach ($options['remove_filter'] as $filter) {
                call_user_func_array('remove_filter', $filter);
            }
        }
        if (array_key_exists('cleanup', $options) && $options['cleanup'] === true) {
            add_action('after_setup_theme', [Theme::class, 'cleanUp']);
        }
        if (array_key_exists('enqueue', $options)) {
            Enqueue::process($options['enqueue']);
        }
        if (array_key_exists('option_page', $options) && $options['option_page'] === true) {
            if (function_exists('acf_add_options_page')) {
                acf_add_options_page();
            }
        }
        if (array_key_exists('register', $options) && !empty($options['register'])) {
            add_action('after_setup_theme', function () use ($options) {
                foreach ($options['register'] as $rawCall) {
                    self::register($rawCall);
                }
            });
        }
    }

    private static function register($rawCall)
    {
        if (!preg_match("/([a-zA-Z:])+\:\:([a-zA-Z])+/", $rawCall)) {
            throw new ThemeException($rawCall . ' not a valid call');
        }

        list($rawController, $methodName) = explode('::', $rawCall, 2);

        $controllerName = "\\" . str_replace(":", "\\", $rawController);

        $controller = new $controllerName();

        return call_user_func_array([$controller, $methodName], []);
    }

    public function cleanUp()
    {
        // EditURI link.
        remove_action('wp_head', 'rsd_link');

        // Category feed links.
        remove_action('wp_head', 'feed_links_extra', 3);

        // Post and comment feed links.
        remove_action('wp_head', 'feed_links', 2);

        // Windows Live Writer.
        remove_action('wp_head', 'wlwmanifest_link');

        // Index link.
        remove_action('wp_head', 'index_rel_link');

        // Previous link.
        remove_action('wp_head', 'parent_post_rel_link', 10, 0);

        // Start link.
        remove_action('wp_head', 'start_post_rel_link', 10, 0);

        // Canonical.
        remove_action('wp_head', 'rel_canonical', 10, 0);

        // Shortlink.
        remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);

        // Links for adjacent posts.
        remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);

        // WP version.
        remove_action('wp_head', 'wp_generator');

        // Emoji detection script.
        remove_action('wp_head', 'print_emoji_detection_script', 7);

        // Emoji styles.
        remove_action('wp_print_styles', 'print_emoji_styles');

        // Remove WP version from RSS.
        add_filter('the_generator', [Theme::class, 'remove_rss_version']);
    }

    public function remove_rss_version()
    {
        return '';
    }
}
