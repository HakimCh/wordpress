<?php

namespace HakimCh\Wordpress\Services;

use HakimCh\Wordpress\Exceptions\ViewException;
use HakimCh\Wordpress\Services\Renderer\Renderer;
use WP_Query;

/**
 * Class View
 * @package HakimCh\Wordpress\Services
 */
class View
{
    /**
     * @var Renderer $engine
     */
    protected $engine;

    /**
     * @var array $globals
     */
    protected $globals = [];

    /**
     * @var string $path
     */
    protected $path = 'views';

    /**
     * @var boolean $initialized
     */
    private $initialized;

    /**
     * Render the template
     *
     * @return string
     */
    public function render()
    {
        if (!$this->initialized) {
            $this->initialize();
        }

        $template = $this->prophecy();

        $this->engine->setGlobals($this->globals);
        $this->engine->setTemplate($template);

        return $this->engine->render();
    }

    /**
     * Predict the template
     *
     * @param SinglePost|null $post
     *
     * @return string
     * @throws ViewException
     */
    private function prophecy()
    {
        $pageType = $this->getPageType();

        if (in_array($pageType, ['single', 'page', 'front'])) {
            $post = new SinglePost();
            $this->globals['post'] = $post;
            $postType = $post->getType();
            if ($template = $post->getTemplate()) {
                $templates = get_page_templates();
                if (array_key_exists($template, $templates)) {
                    $pageType = str_replace('.php', '', $templates[$template]);
                }
            } elseif ($pageType == 'single' && file_exists($singleTypeTemplatePath = $this->path . $postType . '-single.php')) {
                $pageType = $postType . '-single';
            } elseif ($pageType == 'page') {
                $postId = $post->getId();
                $postSlug = $post->getSlug();
                if (file_exists($singleTypeTemplatePath = $this->path . $postType . '-page.php')) {
                    $pageType = $postType . '-page';
                } elseif (file_exists($singleTypeTemplatePath = $this->path . $postId . '-page.php')) {
                    $pageType = $postId . '-page';
                } elseif (file_exists($singleTypeTemplatePath = $this->path . $postSlug . '-page.php')) {
                    $pageType = $postSlug . '-page';
                }
            }
        }

        $templatePath = $this->path . $pageType . '.php';

        if (!file_exists($templatePath)) {
            $templatePath = $this->path . '404.php';
            $this->globals['notfound'] = [
                'title' => 'Page not found',
                'content' => '<strong>' . $pageType . '</strong> page not found'
            ];
        }

        return $templatePath;
    }

    /**
     * Initialize the view params
     *
     * @throws ViewException
     */
    public function initialize()
    {
        $configuration = Configuration::parameters()->get('renderer');

        if (!array_key_exists('engine', $configuration)) {
            throw new ViewException('Please make sure you add an engine to the theme.yml (parameters.renderer.engine)');
        }
        $engine = "HakimCh\\Wordpress\\Services\\Renderer\\" . strtoupper($configuration['engine']) . 'Renderer';
        if (!class_exists($engine)) {
            throw new ViewException('Please make sure the engine exist (' . $engine . ')');
        }
        $this->engine = new $engine();
        if (array_key_exists('globals', $configuration)) {
            $this->globals = $configuration['globals'];
        }
        if (array_key_exists('path', $configuration)) {
            $this->path = $configuration['path'];
        }

        $this->path = get_template_directory() . DIRECTORY_SEPARATOR . $this->path . DIRECTORY_SEPARATOR;

        $this->initialized = true;
    }


    /**
     * Get the template type (front, page,  single...)
     *
     * @return string
     */
    protected function getPageType()
    {
        global $wp_query;
        $pageType = '404';

        if ($wp_query->is_page) {
            $pageType = is_front_page() ? 'front' : 'page';
        } elseif ($wp_query->is_home) {
            $pageType = 'home';
        } elseif ($wp_query->is_single) {
            $pageType = ($wp_query->is_attachment) ? 'attachment' : 'single';
        } elseif ($wp_query->is_category) {
            $pageType = 'category';
        } elseif ($wp_query->is_tag) {
            $pageType = 'tag';
        } elseif ($wp_query->is_tax) {
            $pageType = 'tax';
        } elseif ($wp_query->is_archive) {
            if ($wp_query->is_day) {
                $pageType = 'day';
            } elseif ($wp_query->is_month) {
                $pageType = 'month';
            } elseif ($wp_query->is_year) {
                $pageType = 'year';
            } elseif ($wp_query->is_author) {
                $pageType = 'author';
            } else {
                $pageType = 'archive';
            }
        } elseif ($wp_query->is_search) {
            $pageType = 'search';
        } elseif ($wp_query->is_404) {
            $pageType = '404';
        }

        return $pageType;
    }
}
