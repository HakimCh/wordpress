<?php

namespace HakimCh\Wordpress\Traits;

use HakimCh\Wordpress\Services\SinglePost;

trait Post
{
    protected $searchBar = false;

    public function hasSearchBar()
    {
        return $this->searchBar;
    }

    /**
     * Convert object to array.
     *
     * @return array Object as array.
     */
    public function toArray($object)
    {
        if (is_array($object)) {
            return $object;
        }

        $post = get_object_vars($object);

        foreach (array( 'ancestors', 'page_template', 'post_category', 'tags_input' ) as $key) {
            if (isset($object->$key)) {
                $post[ $key ] = $object->$key;
            }
        }

        return $post;
    }

    public function includeSearchBar($searchBar)
    {
        return $this->searchBar = $searchBar;
    }

    public function getTaxonomies($postType)
    {
        return get_object_taxonomies($postType);
    }

    /**
     * @param SinglePost $post
     *
     * @return string
     */
    public function getBackgroundImage(SinglePost $post)
    {
        $style = '';
        if ($image = $post->getImage()) {
            $style = ' style="background-image: url('.$image.')"';
        }
        return 'class="background-no-image"' . $style;
    }

    /**
     * @param SinglePost $post
     *
     * @return string
     */
    public function getBackgroundColor(SinglePost $post)
    {
        $color = $post->getMeta('background_color');
        if ($color) {
            return ' style="background-color: '.$color.'"';
        }
        return null;
    }
}
