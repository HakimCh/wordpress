<?php

namespace HakimCh\Wordpress\Traits;

use HakimCh\Wordpress\Services\Paginate;
use HakimCh\Wordpress\Services\UserQuery;

trait SearchBar
{

    /**
     * @var string|integer $categoryTerm
     */
    private $categoryTerm;

    /**
     * @var string|integer $searchTerms
     */
    private $searchTerms;

    public function search($cptName)
    {
        $posts = UserQuery::type($cptName);
        if (isset($_POST) && !empty($_POST)) {
            $this->categoryTerm = sanitize_title($_POST['category']);
            $this->searchTerms  = sanitize_title(trim($_POST['search']));
            $posts->where('taxonomy', $this->categoryTerm);
            if (!empty($this->searchTerms)) {
                $posts->where('s', $this->searchTerms);
            }
        }
        return Paginate::getInstance()->make($posts);
    }

    public function pagination()
    {
        return Paginate::getInstance()->generate();
    }

    /**
     * @return mixed
     */
    public function getCategoryTerm()
    {
        return $this->categoryTerm;
    }

    /**
     * @return mixed
     */
    public function getSearchTerms()
    {
        return $this->searchTerms;
    }
}
